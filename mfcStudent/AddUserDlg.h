#pragma once


// CAddUserDlg 对话框

class CAddUserDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CAddUserDlg)

public:
	CAddUserDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CAddUserDlg();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DLG_SCORE };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()

    BOOL is_add;
public:
    afx_msg void OnBnClickedOk();
    afx_msg LRESULT OnRecvMsg(WPARAM wParam,LPARAM lParam);
    afx_msg void OnBnClickedCancel();
    afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
    afx_msg void OnEnKillfocusEditId();
    afx_msg void OnEnKillfocusEditArm();
    virtual BOOL OnInitDialog();
};
