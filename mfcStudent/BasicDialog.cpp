#include "stdafx.h"
#include"Resource.h"
#include "BasicDialog.h"


CBasicDialog::CBasicDialog()
{
    

}

CBasicDialog::CBasicDialog(UINT nIDD, CWnd * pParent)
    :CDialogEx(nIDD,pParent)
{
   
}

CBasicDialog::CBasicDialog(UINT nIDD, CRect rect, CWnd * pParent)
    :CDialogEx(nIDD,pParent)
{
    m_rect = rect;
}

CBasicDialog::CBasicDialog(UINT nIDD, int l, int t, int r, int b, CWnd * pParent)
    :CDialogEx(nIDD,pParent)
{
    m_rect = CRect(l, t, r, b);
}


CBasicDialog::~CBasicDialog()
{
}


BOOL CBasicDialog::OnInitDialog()
{
    CDialogEx::OnInitDialog();
   
    initDialog();
    m_list.Create(WS_CHILD | WS_VISIBLE | WS_BORDER | LVS_REPORT | LVS_EDITLABELS,
        m_rect, this, IDD_MYLISTCTRL);
    m_list.SetBkColor(RGB(186, 156, 128));

    DWORD dwStyle = m_list.GetExtendedStyle();
    dwStyle |= LVS_EX_FULLROWSELECT;    //选中某行使整行高亮（只适用与report风格的listctrl）
    dwStyle |= LVS_EX_GRIDLINES;        //网格线（只适用与report风格的listctrl）
    dwStyle |= LVS_EX_CHECKBOXES;       //item前生成checkbox控件
    m_list.SetExtendedStyle(dwStyle);   //设置扩展风格

    m_edit.Create(ES_MULTILINE | WS_CHILD | WS_VISIBLE | WS_TABSTOP | WS_BORDER,
        m_rect, this, IDC_EDIT_DBCLICK);

    m_edit.ShowWindow(SW_HIDE);
    return TRUE;  // return TRUE unless you set the focus to a control
                  // 异常: OCX 属性页应返回 FALSE
}


void CBasicDialog::DoDataExchange(CDataExchange* pDX)
{
    // TODO: 在此添加专用代码和/或调用基类

    CDialogEx::DoDataExchange(pDX);
}
BEGIN_MESSAGE_MAP(CBasicDialog, CDialogEx)
    ON_WM_CLOSE()
    //ON_NOTIFY(NM_DBLCLK, IDC_LIST_SHOW, &CRootDlg::OnNMDblclkListShow)
    //ON_EN_KILLFOCUS(IDC_EDIT_SHOW, &CRootDlg::OnEnKillfocusEditShow)
    ON_EN_KILLFOCUS(IDC_EDIT_DBCLICK,&CBasicDialog::OnEnKillfocusEditShow)
    ON_NOTIFY(NM_DBLCLK, IDD_MYLISTCTRL,&CBasicDialog::OnNMDblclkListShow)
END_MESSAGE_MAP()


void CBasicDialog::OnClose()
{
    // TODO: 在此添加消息处理程序代码和/或调用默认值

    CDialogEx::OnClose();
}

void CBasicDialog::OnNMDblclkListShow(NMHDR * pNMHDR, LRESULT * pResult)
{
    LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
    CRect rc;
    m_Row = pNMItemActivate->iItem;         //获得选中的行
    m_Col = pNMItemActivate->iSubItem;      //获得选中列

    int flag = 0;
    for (int i = 0; i < colums.size(); i++) {
        if (colums[i] == m_Col) {
            flag = 1;
            break;
        }
    }
    if (!flag) return;

    if (pNMItemActivate->iSubItem != -1)    //如果选择的是子项;
    {
        m_list.GetSubItemRect(m_Row, m_Col, LVIR_LABEL, rc);    //获得子项的RECT；
        m_edit.SetParent(&m_list);  //转换坐标为列表框中的坐标
        m_edit.MoveWindow(rc);  //移动Edit到RECT坐在的位置;
        m_edit.SetWindowText(m_list.GetItemText(m_Row, m_Col)); //将该子项中的值放在Edit控件中；
        m_editStr =m_list.GetItemText(m_Row, m_Col);
        m_edit.ShowWindow(SW_SHOW);     //显示Edit控件；
        m_edit.SetFocus();  //设置Edit焦点
        m_edit.ShowCaret(); //显示光标
        m_edit.SetSel(-1);  //将光标移动到最后
    }
    *pResult = 0;
}

void CBasicDialog::OnEnKillfocusEditShow()
{
    CString tem;
    m_edit.GetWindowText(tem);    //得到用户输入的新的内容
    m_list.SetItemText(m_Row, m_Col, tem);   //设置编辑框的新内容
    m_edit.ShowWindow(SW_HIDE);                //应藏编辑框

    if (m_editStr == tem) return;
    
}

void CBasicDialog::initDialog()
{
    CRect tmpRect;
    GetClientRect(&tmpRect);
    
    if (m_rect.right != BASIC_DIG_LIST_RB && m_rect.bottom == BASIC_DIG_LIST_RB) {
        m_rect = CRect(m_rect.left, m_rect.top, m_rect.right,tmpRect.bottom);
    }
    else if (m_rect.right== BASIC_DIG_LIST_RB && m_rect.bottom == BASIC_DIG_LIST_RB)
    {
        m_rect = CRect(m_rect.left, m_rect.top, tmpRect.right, tmpRect.bottom);
    }
    else {
        m_rect = m_rect;
    }
}

void CBasicDialog::SetListPos(CRect rect)
{
    //m_list.SetProxy()
}

void CBasicDialog::InsertHeader(CStringList& strlist,int colunm_width)
{
    for (int i = 0; i < strlist.GetSize(); i++) {
        m_list.InsertColumn(i, strlist.GetAt(strlist.FindIndex(i)),LVCFMT_LEFT,colunm_width);
    }
}


void CBasicDialog::PreInitDialog()
{
    // TODO: 在此添加专用代码和/或调用基类

    CDialogEx::PreInitDialog();
}

void CBasicDialog::SeteditableColums(vector<int> colums)
{
    this->colums = colums;
}

void CBasicDialog::SetConnectTable(CString table)
{
    this->m_table = table;
}

void CBasicDialog::SetTableFields(CStringList fields)
{
    for (int i = 0; i < fields.GetSize(); i++) {
        m_fields.AddTail(fields.GetAt(fields.FindIndex(i)));
    }
}
