// StudentDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "mfcStudent.h"
#include "StudentDlg.h"
#include "afxdialogex.h"
#include<vector>

// CStudentDlg 对话框

IMPLEMENT_DYNAMIC(CStudentDlg, CDialogEx)

CStudentDlg::CStudentDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_DLG_USER, pParent)
{
    
}

CStudentDlg::~CStudentDlg()
{
}

void CStudentDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialogEx::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_LIST_INFO, m_stuList);
    DDX_Control(pDX, IDC_SUB_HAS, m_SubHas);

}

void CStudentDlg::init_list()
{

    char* strList[8] = {"学号","姓名","学制/每年","重修学分" ,"参军转业学分","总学分","获得学分"};
    CString sql;
    sql.Format(" SELECT student_id,username,edu_systme,reset_score,army_score,score FROM student_info\
 WHERE student_id='%s'", g_logStudent);

    if (g_dataBase.query(sql, SEL)) {
        for (int i = 0; i < g_dataBase.num_fields; i++) {
            m_stuList.InsertItem(LVIF_TEXT | LVIF_STATE, i, "item", LVIS_SELECTED, LVIS_SELECTED, 0, 0);
            
            m_stuList.SetItemText(i, 0, strList[i]);
            m_stuList.SetItemText(i, 1, g_dataBase.m_dataRow[i]);
        }
        sql.Format(" SELECT SUM(my_credit) FROM id_subjects WHERE student_id=%s AND is_reset=0"
            , g_logStudent);
        if (g_dataBase.query(sql, SEL)) {
            m_stuList.InsertItem(LVIF_TEXT | LVIF_STATE, 6, "item", LVIS_SELECTED, LVIS_SELECTED, 0, 0);
            m_stuList.SetItemText(6, 0, strList[6]);
            m_stuList.SetItemText(6, 1, g_dataBase.m_dataRow[0]);
        }
    }
}

void CStudentDlg::update_has_class()
{
    m_SubHas.DeleteAllItems();
    CString sql;
    sql.Format("SELECT u.course_num,s.subject_name,u.score,u.my_credit FROM id_subjects u,subjects s\
 WHERE u.course_num=s.course_num AND u.student_id = '%s'",g_logStudent);
    TRACE("%s:%s", __FUNCTION__, sql.GetBuffer());
    if (g_dataBase.query(sql, SEL))
    {
        for (int i = 0; i < g_dataBase.num_rows; i++)
        {
            m_SubHas.InsertItem(LVIF_TEXT | LVIF_STATE, i,"item", LVIS_SELECTED, LVIS_SELECTED, 0, 0);
            for (int j = 0; j < g_dataBase.num_fields; j++)
            {
                TRACE("#%s:%s", __FUNCTION__, g_dataBase.m_dataRow[j]);
                m_SubHas.SetItemText(i,j, g_dataBase.m_dataRow[j]);
            }
            g_dataBase.mysql_fetch_next_row();
        }

    }
}



void CStudentDlg::insert_header()
{
    m_SubHas.InsertColumn(0, "课程序号", LVCFMT_LEFT, WIDTH+20);
    m_SubHas.InsertColumn(1, "课程名称", LVCFMT_LEFT, WIDTH + 40);
    m_SubHas.InsertColumn(2, "分数", LVCFMT_LEFT, WIDTH);
    m_SubHas.InsertColumn(3, "所得学分", LVCFMT_LEFT, WIDTH);
}


BEGIN_MESSAGE_MAP(CStudentDlg, CDialogEx)
    
END_MESSAGE_MAP()



BOOL CStudentDlg::OnInitDialog()
{
    CDialogEx::OnInitDialog();
    SetBackgroundColor(RGB(254, 212, 0));

    DWORD dwStyle = m_stuList.GetExtendedStyle();
    dwStyle |= LVS_EX_FULLROWSELECT;    //选中某行使整行高亮（只适用与report风格的listctrl）
    dwStyle |= LVS_EX_GRIDLINES;        //网格线（只适用与report风格的listctrl）

    m_stuList.SetExtendedStyle(dwStyle);   //设置扩展风格

    m_SubHas.SetExtendedStyle(dwStyle);

    m_stuList.InsertColumn(0, "", LVCFMT_LEFT, WIDTH);
    m_stuList.InsertColumn(1, "", LVCFMT_LEFT, WIDTH);

    init_list();
    insert_header();

    update_has_class();
    return TRUE;  // return TRUE unless you set the focus to a control
               
}
