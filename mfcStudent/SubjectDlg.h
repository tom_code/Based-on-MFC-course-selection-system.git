#pragma once


// CSubjectDlg 对话框

class CSubjectDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CSubjectDlg)

public:
	CSubjectDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CSubjectDlg();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DLG_COUSE };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
    afx_msg void OnBnClickedOk();
//    afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
    afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
};
