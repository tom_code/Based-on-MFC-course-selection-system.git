#pragma once
#include"stdafx.h"
#include<mysql.h>
using namespace std;

class CDataBase
{
public:

    CDataBase();
    ~CDataBase();
    BOOL init_database();
    BOOL query(CString sql,SQL type);
    BOOL mysql_fetch_next_row();
    BOOL is_has_item(CString table,CString item, CString value);
    BOOL mysql_free();
public:
    MYSQL* m_mysql;
    MYSQL_ROW m_dataRow;
    MYSQL_RES* mysql_res;
    unsigned int num_rows;      //结果行数
    unsigned int num_fields;    //结果集列数

};

