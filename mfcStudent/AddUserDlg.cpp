// AddUserDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "mfcStudent.h"
#include "AddUserDlg.h"
#include "afxdialogex.h"


// CAddUserDlg 对话框

IMPLEMENT_DYNAMIC(CAddUserDlg, CDialogEx)

CAddUserDlg::CAddUserDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_DLG_SCORE, pParent)
{
    is_add = true;
    SetBackgroundColor(RGB(254, 212, 0));

}

CAddUserDlg::~CAddUserDlg()
{
}

void CAddUserDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CAddUserDlg, CDialogEx)
    ON_BN_CLICKED(IDOK, &CAddUserDlg::OnBnClickedOk)
    ON_MESSAGE(WM_TO_SON, &CAddUserDlg::OnRecvMsg)
    ON_BN_CLICKED(IDCANCEL, &CAddUserDlg::OnBnClickedCancel)
    ON_WM_SHOWWINDOW()
    ON_EN_KILLFOCUS(IDC_EDIT_ID, &CAddUserDlg::OnEnKillfocusEditId)
    ON_EN_KILLFOCUS(IDC_EDIT_ARM, &CAddUserDlg::OnEnKillfocusEditArm)
END_MESSAGE_MAP()


// CAddUserDlg 消息处理程序


void CAddUserDlg::OnBnClickedOk()
{
    CString id, name, arm_score, pass_code;
    
    GetDlgItemText(IDC_EDIT_ID, id);
    if (id.IsEmpty()) {
        MessageBox("请输入学号");
        return;
    }
    GetDlgItemText(IDC_EDIT_NAME, name);
    BOOL res;
    int year=GetDlgItemInt(IDC_EDIT_YEAR, &res,FALSE);
    arm_score.Format("%d", ((CButton*)GetDlgItem(IDC_RADIO_NO))->GetCheck() ? 0 : 5);
    GetDlgItemText(IDC_EDIT_CODE, pass_code);

    CString sql;
    if (query_do == ALE) {  //更新
        sql.Format("UPDATE student_info SET username='%s',\
            edu_systme=%d,passwd = %s,army_score=%f WHERE student_id = '%s'",
            name,year,pass_code,atof(arm_score),id);

        TRACE("#%s:%s", __FUNCTION__, sql.GetBuffer());
        if (g_dataBase.query(sql.GetBuffer(),ALE))
        {
            MessageBox("执行成功", "提示", MB_OK);
            WPARAM wParam = SQL_SUCCESS;
            LPARAM lParam = 9;
            GetParent()->SendMessage(WM_MYMSG, wParam, lParam);
            query_do = NO;
        }
        else  
        {
            MessageBox("执行失败", "提示", MB_OK);
            return;
        }
    }
    else  //插入
    {
        if (g_dataBase.is_has_item("student_info", "student_id", id)) {
            CString str;
            str.Format("学号:%s已存在!",id);
            MessageBox(str, "提示", MB_OK);
            return;
        }
        
        sql.Format("INSERT INTO student_info VALUE(NULL,'%s','%s',NULL,NULL,%d,%f,'%s')",
            id, name, year, atof(arm_score), pass_code);
        TRACE("#%s:%s", __FUNCTION__, sql.GetBuffer());
        
        if (g_dataBase.query(sql, ADD))
        {
            MessageBox("执行成功", "提示", MB_OK);
            WPARAM wParam = SQL_SUCCESS;
            LPARAM lParam = 9;
            GetParent()->SendMessage(WM_MYMSG, wParam, lParam);
        }
        else
        {
            MessageBox("执行失败", "提示", MB_OK);
            return;
        }
    }
   
    CDialogEx::OnOK();
}

LRESULT CAddUserDlg::OnRecvMsg(WPARAM wParam, LPARAM lParam)
{
    TRACE("#%s:%d", __FUNCTION__,wParam);
    return LRESULT();
}


void CAddUserDlg::OnBnClickedCancel()
{
    
    CDialogEx::OnCancel();
}


void CAddUserDlg::OnShowWindow(BOOL bShow, UINT nStatus)
{
    CDialogEx::OnShowWindow(bShow, nStatus);
    SetBackgroundColor(RGB(254, 212, 0));
    if (FALSE == school_id.IsEmpty()) {
        CString sql;
        sql.Format("SELECT * FROM student_info WHERE student_id='%s'", school_id);
        if (g_dataBase.query(sql, SEL)) {
            if (g_dataBase.num_rows == 0) {
                TRACE("%s:%s", __FUNCTION__, "row==0");
                return;
            }
            this->SetWindowText("修改学生信息");
            is_add = false;
            GetDlgItem(IDC_EDIT_ID)->SetWindowText(g_dataBase.m_dataRow[1]);
            ((CEdit*)GetDlgItem(IDC_EDIT_ID))->SetReadOnly(TRUE);
            GetDlgItem(IDC_EDIT_NAME)->SetWindowText(g_dataBase.m_dataRow[2]);
            
            GetDlgItem(IDC_EDIT_YEAR)->SetWindowText(g_dataBase.m_dataRow[5]);
            GetDlgItem(IDC_EDIT_CODE)->SetWindowText(g_dataBase.m_dataRow[7]);
            ((CButton*)GetDlgItem(IDC_RADIO_NO))->SetCheck(atoi(g_dataBase.m_dataRow[6]) == 5 ? 0 : 1);
            query_do = ALE;
        }
        school_id.Empty();
    }
}


void CAddUserDlg::OnEnKillfocusEditId()
{
    CString strID;
    GetDlgItemText(IDC_EDIT_ID, strID);
    SetDlgItemText(IDC_EDIT_CODE, strID.Right(4));
    SetDlgItemText(IDC_EDIT_YEAR, "4");

}


void CAddUserDlg::OnEnKillfocusEditArm()
{
    CString student_num,reset_score, army_score,sum_score;
    GetDlgItemText(IDC_EDIT_ID, student_num);
    GetDlgItemText(IDC_EDIT_RESET, reset_score);
    GetDlgItemText(IDC_EDIT_ARM, army_score);
    sum_score.Format("%d", atoi(reset_score) + atoi(army_score));
    
    CString sql;
    sql.Format("SELECT SUM(credit)  FROM id_subjects WHERE student_id=%s",
        student_num);
    if (g_dataBase.query(sql, SEL)) {
        sum_score.Format("%d", atoi(sum_score) + g_dataBase.m_dataRow[0]);
        SetDlgItemText(IDC_EDIT_SCORE, sum_score);
    }
    UpdateData(TRUE);
}


BOOL CAddUserDlg::OnInitDialog()
{
    CDialogEx::OnInitDialog();

    ((CButton*)GetDlgItem(IDC_RADIO_NO))->SetCheck(TRUE);

    return TRUE;  // return TRUE unless you set the focus to a control
                  // 异常: OCX 属性页应返回 FALSE
}
