#pragma once
#include "BasicDialog.h"
class CTeacherDialog :
    public CBasicDialog
{
public:
    CTeacherDialog();
    CTeacherDialog(UINT nIDD, CWnd* pParent = NULL);
    CTeacherDialog(UINT nIDD,CRect rect,CWnd* pParent=NULL);
    CTeacherDialog(UINT nIDD, int l, int t, int r = BASIC_DIG_LIST_RB, int b = BASIC_DIG_LIST_RB, CWnd* pParent = NULL);
    virtual ~CTeacherDialog();
    virtual BOOL OnInitDialog();
#ifdef AFX_DESIGN_TIME
    enum { IDD = IDD_DLG_TEACHER};
#endif
public:
    void init_dialog();
    void list_update();
    DECLARE_MESSAGE_MAP()

    afx_msg void OnEnKillfocusEditShow();
private:
    float m_score;        //本门课学分
    int m_courseNum;    //课程序号
};

