// SubjectDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "mfcStudent.h"
#include "SubjectDlg.h"
#include "afxdialogex.h"


// CSubjectDlg 对话框

IMPLEMENT_DYNAMIC(CSubjectDlg, CDialogEx)

CSubjectDlg::CSubjectDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_DLG_COUSE, pParent)
{

}

CSubjectDlg::~CSubjectDlg()
{
}

void CSubjectDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CSubjectDlg, CDialogEx)
    ON_BN_CLICKED(IDOK, &CSubjectDlg::OnBnClickedOk)
ON_WM_SHOWWINDOW()
END_MESSAGE_MAP()


// CSubjectDlg 消息处理程序


void CSubjectDlg::OnBnClickedOk()
{
    CString course_num, subject_name, teacher, credits, class_time;
    GetDlgItemText(IDC_EDIT_ID, course_num);
    GetDlgItemText(IDC_EDIT_NAME, subject_name);
    GetDlgItemText(IDC_EDIT_TEACER, teacher);
    GetDlgItemText(IDC_EDIT_CSCOR, credits);
    GetDlgItemText(IDC_EDIT_TIME, class_time);

    CString sql;
    if (query_do == ALE)
    {
        sql.Format("UPDATE subjects SET subject_name='%s',"
            "teacher='%s',credits=%f,class_time=%d WHERE course_num=%d",
            subject_name, teacher, atof(credits), atoi(class_time), atoi(course_num));

        TRACE("#%s:%s", __FUNCTION__, sql.GetBuffer());
        if (g_dataBase.query(sql, ALE))
        {
            MessageBox("执行成功", "提示", MB_OK);
            WPARAM wParam = SQL_SUCCESS;
            LPARAM lParam = SUB_WIN_CLASS;
            GetParent()->SendMessage(WM_MYMSG, wParam, lParam);
            query_do = NO;
        }
        else
        {
            MessageBox("执行失败", "提示", MB_OK);
            return;
        }
    }
    else
    {
        if (g_dataBase.is_has_item("subjects", "course_num", course_num)) {
            CString str;
            str.Format("课程序号:%s已存在!", course_num);
            MessageBox(str, "提示", MB_OK);
            return;
        }
        sql.Format("INSERT INTO subjects VALUE(NULL,%d,'%s','%s',%f,%d)",
            atoi(course_num), subject_name, teacher, atof(credits), atoi(class_time));
        TRACE("%s:%s", __FUNCTION__, sql.GetBuffer());
        if (g_dataBase.query(sql, ADD))
        {
            MessageBox("执行成功", "提示", MB_OK);
            WPARAM wParam = SQL_SUCCESS;
            LPARAM lParam = SUB_WIN_CLASS;
            GetParent()->SendMessage(WM_MYMSG, wParam, lParam);
        }
        else
        {
            MessageBox("执行失败", "提示", MB_OK);
            return;
        }
    }
    
    CDialogEx::OnOK();
}


void CSubjectDlg::OnShowWindow(BOOL bShow, UINT nStatus)
{
    CDialogEx::OnShowWindow(bShow, nStatus);
    if (course_id == 0) return;
    
    CString sql;
    sql.Format("SELECT * FROM subjects WHERE course_num=%d", course_id);
    if (g_dataBase.query(sql, SEL)) {
        if (g_dataBase.num_rows == 0) {
            TRACE("%s:%s", __FUNCTION__, "row==0");
            return;
        }
        this->SetWindowText("修改课程信息");
        GetDlgItem(IDC_EDIT_ID)->SetWindowText(g_dataBase.m_dataRow[1]);
        ((CEdit*)GetDlgItem(IDC_EDIT_ID))->SetReadOnly(TRUE);
        GetDlgItem(IDC_EDIT_NAME)->SetWindowText(g_dataBase.m_dataRow[1]);
        GetDlgItem(IDC_EDIT_TEACER)->SetWindowText(g_dataBase.m_dataRow[3]);
        GetDlgItem(IDC_EDIT_CSCOR)->SetWindowText(g_dataBase.m_dataRow[4]);
        GetDlgItem(IDC_EDIT_TIME)->SetWindowText(g_dataBase.m_dataRow[5]);
        query_do = ALE;
    }
    course_id = 0;
}
