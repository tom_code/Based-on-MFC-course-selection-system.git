#pragma once
#include "afxdialogex.h"
#include<vector>
class CBasicDialog :
    public CDialogEx
{
public:
    CBasicDialog();
    CBasicDialog(UINT nIDD, CWnd* pParent = NULL);
    CBasicDialog(UINT nIDD, CRect rect, CWnd* pParent = NULL);
    CBasicDialog(UINT nIDD,int l,int t,int r= BASIC_DIG_LIST_RB,int b= BASIC_DIG_LIST_RB,CWnd* pParent=NULL);
    ~CBasicDialog();
protected:
    CEdit m_edit;         //编辑控件
    CRect m_rect;        //list 控件显示的位置
    int m_Row;          //点击的行
    int m_Col;          //点击的列
    CString m_editStr;  //编辑前的内容
    vector<int> colums; //可以编辑的列
    CString m_table;    //关联的数据表
    CStringList m_fields; //表中的字段
protected:
    CListCtrl m_list;
public:
    virtual BOOL OnInitDialog();
    virtual void DoDataExchange(CDataExchange* pDX);

    //消息处理函数
    DECLARE_MESSAGE_MAP()
    afx_msg void OnClose();
    virtual afx_msg void OnNMDblclkListShow(NMHDR *pNMHDR, LRESULT *pResult);
    virtual afx_msg void OnEnKillfocusEditShow();
public:
    void initDialog();
    void SetListPos(CRect rect);
    void InsertHeader(CStringList& strlist, int colunm_width);
    virtual void PreInitDialog();
    void SeteditableColums(vector<int> colums);
    void SetConnectTable(CString table);
    void SetTableFields(CStringList fields);
};

