﻿// RootDlg.cpp : 实现文件
//

#include"stdafx.h"
#include"mfcStudent.h"
#include"RootDlg.h"
#include"afxdialogex.h"

CString school_id;      //学号
int   course_id = 0;    //课程序号
SQL  query_do = NO;
BOOL is_stu_addco;
// CRootDlg 对话框


IMPLEMENT_DYNAMIC(CRootDlg, CDialogEx)

CRootDlg::CRootDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_DLG_ROOT, pParent)
{
    m_SubaddUer = nullptr;
    m_SubaddUer = new CAddUserDlg(this);
    m_SubjectDlg = nullptr;
    m_SubjectDlg = new CSubjectDlg(this);
}

CRootDlg::~CRootDlg()
{
    delete m_SubaddUer;
    delete m_SubjectDlg;
}

void CRootDlg::update_list()
{
    update_all_score();
    m_list.DeleteAllItems();
    if(g_dataBase.query("SELECT * FROM  student_info",SEL))
    { 
        TRACE("#list row:%d col:%d", g_dataBase.num_rows, g_dataBase.num_fields);
        CString str;
        for (int i = 0; i < g_dataBase.num_rows; i++)
        {
            m_list.InsertItem(LVIF_TEXT | LVIF_STATE, i,"item",LVIS_SELECTED, LVIS_SELECTED, 0, 0);
            for (int j = 1; j < g_dataBase.num_fields-1; j++)
            {
                str.Format("%d", i + 1);
                m_list.SetItemText(i, 0,str.GetBuffer());
                m_list.SetItemText(i, j, g_dataBase.m_dataRow[j]);
            }
            g_dataBase.mysql_fetch_next_row();
        }
    }
}

void CRootDlg::update_list_search()
{
    m_list.DeleteAllItems();
    CString str;
    for (int i = 0; i < g_dataBase.num_rows; i++)
    {
        m_list.InsertItem(LVIF_TEXT | LVIF_STATE, i, "item", LVIS_SELECTED, LVIS_SELECTED, 0, 0);
        for (int j = 1; j < g_dataBase.num_fields - 1; j++)
        {
            str.Format("%d", i + 1);
            m_list.SetItemText(i, 0, str.GetBuffer());
            m_list.SetItemText(i, j, g_dataBase.m_dataRow[j]);
        }
        g_dataBase.mysql_fetch_next_row();
    }
}

void CRootDlg::update_list_search_class()
{
    m_list.DeleteAllItems();
    for (int i = 0; i < g_dataBase.num_rows; i++)
    {
        m_list.InsertItem(LVIF_TEXT | LVIF_STATE, i, "item", LVIS_SELECTED, LVIS_SELECTED, 0, 0);
        for (int j = 0; j < g_dataBase.num_fields; j++)
        {
            m_list.SetItemText(i, j, g_dataBase.m_dataRow[j]);
        }
        g_dataBase.mysql_fetch_next_row();
    }
}

void CRootDlg::update_subjects()
{
    m_RSubAll.DeleteAllItems();
    if (g_dataBase.query("SELECT * FROM  subjects", SEL))
    {
        TRACE("#list row:%d col:%d", g_dataBase.num_rows, g_dataBase.num_fields);
        CString str;
        for (int i = 0; i < g_dataBase.num_rows; i++)
        {
            m_RSubAll.InsertItem(LVIF_TEXT | LVIF_STATE, i, "item", LVIS_SELECTED, LVIS_SELECTED, 0, 0);
            for (int j = 1; j < g_dataBase.num_fields; j++)
            {
                str.Format("%d", i + 1);
                m_RSubAll.SetItemText(i, 0, str.GetBuffer());
                m_RSubAll.SetItemText(i, j, g_dataBase.m_dataRow[j]);
                if(j==2) TRACE("#%s:%s", __FUNCTION__, g_dataBase.m_dataRow[j]);
            }
            g_dataBase.mysql_fetch_next_row();
        }
    }
}

void CRootDlg::insert_header()
{
    m_RSubAll.InsertColumn(0, "编号", LVCFMT_LEFT, WIDTH-40);
    m_RSubAll.InsertColumn(1, "课程序号", LVCFMT_LEFT, WIDTH);
    m_RSubAll.InsertColumn(2, "课程名称", LVCFMT_LEFT, WIDTH+20);
    m_RSubAll.InsertColumn(3, "教师", LVCFMT_LEFT, WIDTH);
    m_RSubAll.InsertColumn(4, "学分", LVCFMT_LEFT, WIDTH);
    m_RSubAll.InsertColumn(5, "学时", LVCFMT_LEFT, WIDTH);
    
    m_ListShow.InsertColumn(0, "课程名称", LVCFMT_LEFT, WIDTH+40);
    m_ListShow.InsertColumn(1, "课程序号", LVCFMT_LEFT, WIDTH);
    m_ListShow.InsertColumn(2, "分数", LVCFMT_LEFT, WIDTH);
    m_ListShow.InsertColumn(3, "已获学分", LVCFMT_LEFT, WIDTH);
    m_ListShow.InsertColumn(4, "总学分", LVCFMT_LEFT, WIDTH);
    m_ListShow.InsertColumn(5, "是否重修", LVCFMT_LEFT, WIDTH);
}

void CRootDlg::insert_student_header()
{
    m_list.InsertColumn(0, "编号", LVCFMT_LEFT, WIDTH - 20);
    m_list.InsertColumn(1, "学号", LVCFMT_LEFT, WIDTH);
    m_list.InsertColumn(2, "姓名", LVCFMT_LEFT, WIDTH);
    m_list.InsertColumn(3, "学分", LVCFMT_LEFT, WIDTH);
    m_list.InsertColumn(4, "重修学分", LVCFMT_LEFT, WIDTH + 20);
    m_list.InsertColumn(5, "学制/每年", LVCFMT_LEFT, WIDTH);
    m_list.InsertColumn(6, "参军转业学分", LVCFMT_LEFT, WIDTH + 20);
}

void CRootDlg::insert_search_header()
{
    m_list.InsertColumn(0, "学号", LVCFMT_LEFT, WIDTH);
    m_list.InsertColumn(1, "姓名", LVCFMT_LEFT, WIDTH);
    m_list.InsertColumn(2, "分数", LVCFMT_LEFT, WIDTH);
    m_list.InsertColumn(3, "学分", LVCFMT_LEFT, WIDTH);
}

void CRootDlg::delete_list_all_colums()
{
    int nColumnCount = m_list.GetHeaderCtrl()->GetItemCount();
    // Delete all of the columns.
    for (int i = 0; i < nColumnCount; i++)
    {
        m_list.DeleteColumn(0);
    }
}

void CRootDlg::insert_per_score(CString student_id)
{
    m_ListShow.DeleteAllItems();
    CString str;
    str.Format("%s(%s)已选修课程详情:", m_list.GetItemText(m_list_click_row, 2), student_id);
    SetDlgItemText(IDC_STATIC_NAME, str);
    CString sql_class;
    sql_class.Format("SELECT s.subject_name,u.course_num,u.score,u.my_credit,s.credits,u.is_reset FROM id_subjects u,subjects s \
		WHERE u.course_num= s.course_num AND u.student_id ='%s'", student_id);
    if (g_dataBase.query(sql_class, SEL))
    {
        for (int i = 0; i < g_dataBase.num_rows; i++)
        {
            m_ListShow.InsertItem(LVIF_TEXT | LVIF_STATE, i, "item", LVIS_SELECTED, LVIS_SELECTED, 0, 0);
            for (int j = 0; j < g_dataBase.num_fields-1; j++)
            {
                m_ListShow.SetItemText(i, j, g_dataBase.m_dataRow[j]);
            }
            m_ListShow.SetItemText(i, 5, atoi(g_dataBase.m_dataRow[5])?"是":"否");
            g_dataBase.mysql_fetch_next_row();
        }
        g_dataBase.mysql_free();
    }
    
}

void CRootDlg::update_sum_score(CString student_id)
{

    CString sum_temp, sql,sum;
    sql.Format("SELECT SUM(my_credit) FROM id_subjects WHERE student_id='%s'",
        student_id);
    if (g_dataBase.query(sql, SEL)) {
        if (g_dataBase.num_rows < 1) return;
        sum_temp = g_dataBase.m_dataRow[0];
        sql.Format("SELECT army_score FROM student_info WHERE student_id='%s'",
            student_id);
        if (g_dataBase.query(sql, SEL)) {
            if (g_dataBase.num_rows < 1) return;
            sum.Format("%f", atof(sum_temp) + atof(g_dataBase.m_dataRow[0]));
            sql.Format("UPDATE student_info SET score=%f WHERE student_id='%s'",
                atof(sum), student_id);
            if (g_dataBase.query(sql, ALE)) {
                TRACE("%s:OK",__FUNCTION__);
            }
        }
    }

    
}

void CRootDlg::update_reset_score(CString student_id)
{
    CString sum_temp, sql;
    sql.Format("SELECT SUM(my_credit) AS sum_temp  FROM id_subjects \
WHERE student_id='%s' AND is_reset = 1",
        student_id);
    if (g_dataBase.query(sql, SEL)) {
        sum_temp = g_dataBase.m_dataRow[0];
        sql.Format("UPDATE student_info SET reset_score=%f WHERE student_id=%s",
            atof(sum_temp),student_id);
        if (g_dataBase.query(sql,ALE)) {
            TRACE("#OK");
        }
    }
}

void CRootDlg::update_all_score()
{
    int rows;
    if (g_dataBase.query("SELECT student_id FROM student_info", SEL)) {
        rows = g_dataBase.num_rows;
    }
    CStringList student_ids;
    for (int i = 0; i < rows; i++) {
        student_ids.AddTail(g_dataBase.m_dataRow[0]);
        g_dataBase.mysql_fetch_next_row();
    }
    for (int i = 0; i < rows; i++) {
        update_reset_score(student_ids.GetAt(student_ids.FindIndex(i)));
    }
    for (int i = 0; i < rows; i++) {
        update_sum_score(student_ids.GetAt(student_ids.FindIndex(i)));
    }
}

int CRootDlg::get_student_row()
{
    for (int i = 0; i < m_list.GetItemCount(); i++) {
        if (m_list.GetCheck(i)) {
            return i;
            break;
        }
    }
    return -1;
}

void CRootDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialogEx::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_LIST_USER, m_list);

    DDX_Control(pDX, IDC_RSUBALL, m_RSubAll);
    DDX_Control(pDX, IDC_LIST_SHOW, m_ListShow);
    DDX_Control(pDX, IDC_EDIT_SHOW, m_edit);
}


BEGIN_MESSAGE_MAP(CRootDlg, CDialogEx)
    ON_NOTIFY(NM_DBLCLK, IDC_LIST_USER, &CRootDlg::OnNMDblclkListUser)
    ON_BN_CLICKED(IDC_BTN_ADD, &CRootDlg::OnBnClickedBtnAdd)
    ON_BN_CLICKED(IDC_BTN_DEL, &CRootDlg::OnBnClickedBtnDel)
    ON_BN_CLICKED(IDOK, &CRootDlg::OnBnClickedOk)
    ON_MESSAGE(WM_MYMSG, &CRootDlg::OnUpdateList)   //自定义消息
    ON_BN_CLICKED(IDC_BTN_RADD, &CRootDlg::OnBnClickedBtnRadd)
    ON_BN_CLICKED(IDC_BTN_RDEL, &CRootDlg::OnBnClickedBtnRdel)
    ON_NOTIFY(NM_DBLCLK, IDC_RSUBALL, &CRootDlg::OnNMDblclkRsuball)
    ON_BN_CLICKED(IDC_BTN_SEARCH, &CRootDlg::OnBnClickedBtnSearch)
    ON_NOTIFY(NM_CLICK, IDC_LIST_USER, &CRootDlg::OnNMClickListUser)
    ON_NOTIFY(NM_DBLCLK, IDC_LIST_SHOW, &CRootDlg::OnNMDblclkListShow)

    ON_NOTIFY(NM_CLICK, IDC_LIST_SHOW, &CRootDlg::OnNMClickListShow)
    ON_EN_KILLFOCUS(IDC_EDIT_SHOW, &CRootDlg::OnEnKillfocusEditShow)
    ON_BN_CLICKED(IDC_BTN_ADDS, &CRootDlg::OnBnClickedBtnAdds)
    
    ON_BN_CLICKED(IDC_BTN_SEARCH_STU, &CRootDlg::OnBnClickedBtnSearchStu)
    ON_COMMAND(ID_MENU_DEL, &CRootDlg::OnMenuDel)
    ON_NOTIFY(NM_RCLICK, IDC_LIST_SHOW, &CRootDlg::OnNMRClickListShow)
END_MESSAGE_MAP()


// CRootDlg 消息处理程序


BOOL CRootDlg::OnInitDialog()
{
    CDialogEx::OnInitDialog();
    SetBackgroundColor(RGB(254, 212, 0));

    //WS_OVERLAPPED | WS_SYSMENU | WS_BORDER;
    DWORD wndStyle = GetExStyle();
    wndStyle |= WS_OVERLAPPED;
    wndStyle |= WS_SYSMENU;
    wndStyle |= WS_BORDER;
    
    ModifyStyle(0,wndStyle);

    DWORD dwStyle = m_list.GetExtendedStyle();
    dwStyle |= LVS_EX_FULLROWSELECT;    //选中某行使整行高亮（只适用与report风格的listctrl）
    dwStyle |= LVS_EX_GRIDLINES;        //网格线（只适用与report风格的listctrl）
    dwStyle |= LVS_EX_CHECKBOXES;       //item前生成checkbox控件
    m_list.SetExtendedStyle(dwStyle);   //设置扩展风格
    m_RSubAll.SetExtendedStyle(dwStyle);
    m_ListShow.SetExtendedStyle(dwStyle);

    DWORD style = m_ListShow.GetExtendedStyle();
    style |= LVS_EX_FULLROWSELECT;
    style|= LVS_EX_GRIDLINES;
    m_ListShow.SetExtendedStyle(style);

    insert_header();
    insert_student_header();
    update_list();
    update_subjects();
    update_all_score();
    m_edit.ShowWindow(SW_HIDE);
    return TRUE;  // return TRUE unless you set the focus to a control
                  // 异常: OCX 属性页应返回 FALSE
}


BOOL CRootDlg::PreCreateWindow(CREATESTRUCT& cs)
{
    // Call the base-class version
    if (!CRootDlg::PreCreateWindow(cs))
        return FALSE;

    // Create a window without min/max buttons or sizable border 
    cs.style = WS_OVERLAPPED | WS_SYSMENU | WS_BORDER;

    return TRUE;


    return CDialogEx::PreCreateWindow(cs);
}

//双击
void CRootDlg::OnNMDblclkListUser(NMHDR *pNMHDR, LRESULT *pResult)
{
    LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
    if (pNMItemActivate->iItem != -1)
    {
        TRACE("#%s:%d %d",
           __FUNCTION__,pNMItemActivate->iItem, pNMItemActivate->iSubItem);
        //m_SubaddUer->ShowWindow(SW_SHOW);   //显示
        if (m_SubaddUer == nullptr) {
            ASSERT("m_SunaddUer==null");
            return;
        }
        school_id = m_list.GetItemText(pNMItemActivate->iItem, 1);
        m_SubaddUer->DoModal();
    }
    *pResult = 0;
}


void CRootDlg::OnBnClickedBtnAdd()
{
    if (m_SubaddUer == nullptr) {
        ASSERT("m_SunaddUer==null");
        return;
    }
    m_SubaddUer->DoModal();
}


void CRootDlg::OnBnClickedBtnDel()
{
    int items = m_list.GetItemCount();
    if (items <= 0) {
        MessageBox("请勾选删除项","提示_", MB_OK);
        return;
    }
    CString sql;
    for (int i = 0; i < items; i++)
    {
        if (m_list.GetCheck(i))
        {
            CString strItem=m_list.GetItemText(i, 1);
            sql.Format("DELETE FROM student_info WHERE student_id='%s'", strItem);
            g_dataBase.query(sql,DEL);
        }
    }
    update_list();
}


void CRootDlg::OnBnClickedOk()
{
    // TODO: 在此添加控件通知处理程序代码
    CDialogEx::OnOK();
}

LRESULT CRootDlg::OnUpdateList(WPARAM wParam, LPARAM lParam)
{
    TRACE("#%s:接收到子窗口消息", __FUNCTION__);
    TRACE("#%s:%d %d", __FUNCTION__,wParam,lParam);
    if (wParam == SQL_SUCCESS) {
        update_list();
    }
    if (lParam == SUB_WIN_CLASS) {
        update_subjects();
    }
    
    return LRESULT();
}




void CRootDlg::OnBnClickedBtnRadd()
{
    m_SubjectDlg->DoModal();
}

void CRootDlg::OnBnClickedBtnRdel()
{
    int items = m_RSubAll.GetItemCount();
    if (items <= 0) {
        MessageBox("请勾选删除项", "提示_", MB_OK);
        return;
    }
    CString sql;
    for (int i = 0; i < items; i++)
    {
        if (m_RSubAll.GetCheck(i))
        {
            CString strItem = m_RSubAll.GetItemText(i, 1);
            sql.Format("DELETE FROM subjects WHERE course_num=%d", atoi(strItem));
            g_dataBase.query(sql.GetBuffer(), DEL);
        }
    }
    update_subjects();
}


void CRootDlg::OnNMDblclkRsuball(NMHDR *pNMHDR, LRESULT *pResult)
{
    LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
    if (pNMItemActivate->iItem != -1)
    {
        TRACE("#%s:%d %d",
            __FUNCTION__, pNMItemActivate->iItem, pNMItemActivate->iSubItem);
        if (m_SubjectDlg == nullptr) {
            ASSERT("m_SunaddUer==null");
            return;
        }
        course_id = atoi(m_RSubAll.GetItemText(pNMItemActivate->iItem, 1));
        m_SubjectDlg->DoModal();
    }
    *pResult = 0;
}


void CRootDlg::OnBnClickedBtnSearch()
{
    CString search;
    GetDlgItemText(IDC_EDIT_SEARCH, search);
    if (search.IsEmpty())
    {
        update_subjects();
        return;
    }
    else  //非空
    {
        CString sql;
        sql.Format("SELECT * FROM subjects WHERE course_num=%d", atoi(search));
        TRACE("%s:%s", __FUNCTION__, sql);
        if (g_dataBase.query(sql, SEL))
        {
            if (g_dataBase.num_rows == 0)
            {
                MessageBox("没有找到符合条件的课程", "提示", MB_OK);
                return;
            }
            m_RSubAll.DeleteAllItems();
            CString str;
            for (int i = 0; i < g_dataBase.num_rows; i++)
            {
                m_RSubAll.InsertItem(LVIF_TEXT | LVIF_STATE, i, "item", LVIS_SELECTED, LVIS_SELECTED, 0, 0);
                for (int j = 1; j < g_dataBase.num_fields; j++)
                {
                    str.Format("%d", i + 1);
                    m_RSubAll.SetItemText(i, 0, str.GetBuffer());
                    m_RSubAll.SetItemText(i, j, g_dataBase.m_dataRow[j]);
                    if (j == 2) TRACE("#%s:%s", __FUNCTION__, g_dataBase.m_dataRow[j]);
                }
                g_dataBase.mysql_fetch_next_row();
            }
        }
        else
        {
            MessageBox("执行失败", "提示", MB_OK);
            return;
        }
    }
}


void CRootDlg::OnNMClickListUser(NMHDR *pNMHDR, LRESULT *pResult)
{
    LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
    if (pNMItemActivate->iItem != -1)
    {
        TRACE("#%s:%d %d",
            __FUNCTION__, pNMItemActivate->iItem, pNMItemActivate->iSubItem);
        if (m_SubaddUer == nullptr) {
            ASSERT("m_SunaddUer==null");
            return;
        }
        m_student_num = m_list.GetItemText(pNMItemActivate->iItem, 1);
        m_list_click_row = pNMItemActivate->iItem;
        CString student_id= m_list.GetItemText(pNMItemActivate->iItem, 1);
        insert_per_score(student_id);
    }
    *pResult = 0;
}


void CRootDlg::OnNMDblclkListShow(NMHDR *pNMHDR, LRESULT *pResult)
{
    LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
    CRect rc;
    m_Row = pNMItemActivate->iItem;//获得选中的行
    m_Col = pNMItemActivate->iSubItem;//获得选中列
    if (m_Col!=2)  return;
    if (pNMItemActivate->iSubItem != -1) //如果选择的是子项;
    {
        m_ListShow.GetSubItemRect(m_Row, m_Col, LVIR_LABEL, rc);//获得子项的RECT；
        m_edit.SetParent(&m_ListShow);//转换坐标为列表框中的坐标
        m_edit.MoveWindow(rc);//移动Edit到RECT坐在的位置;
        m_edit.SetWindowText(m_ListShow.GetItemText(m_Row, m_Col));//将该子项中的值放在Edit控件中；
        m_editStr = m_ListShow.GetItemText(m_Row, m_Col);
        m_edit.ShowWindow(SW_SHOW);//显示Edit控件；
        m_edit.SetFocus();//设置Edit焦点
        m_edit.ShowCaret();//显示光标
        m_edit.SetSel(-1);//将光标移动到最后
    }
    *pResult = 0;
}


void CRootDlg::OnNMClickListShow(NMHDR *pNMHDR, LRESULT *pResult)
{
    LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
   
    *pResult = 0;
}


void CRootDlg::OnEnKillfocusEditShow()
{
    CString tem,course,credit;
    m_edit.GetWindowText(tem);    //得到用户输入的新的内容
    if (tem == m_editStr) {
        m_edit.ShowWindow(SW_HIDE);                //应藏编辑框
        return;
    }
    TRACE("#:%s:%s", __FUNCTION__, tem);
    m_ListShow.SetItemText(m_Row, m_Col, tem);   //设置编辑框的新内容
    m_edit.ShowWindow(SW_HIDE);                //应藏编辑框

    if (m_Col == 2) {
        credit = m_ListShow.GetItemText(m_Row, 4);
        course = m_ListShow.GetItemText(m_Row, 1);
        CString sql;
        float score = atof(tem) >= 60 ? atof(credit) : 0;
        sql.Format("UPDATE id_subjects SET my_credit=%.1f,score=%.1f WHERE student_id=%s AND course_num=%d", 
            score, atof(tem), m_student_num,atoi(course));

        if (g_dataBase.query(sql, ALE))
        {
            MessageBox("修改成功", "提示", MB_OK);
            update_list();
            insert_per_score(m_student_num);
        }
        else
        {
            MessageBox("修改失败", "提示", MB_OK);
        }
    }
}


void CRootDlg::OnBnClickedBtnAdds()
{
    if (get_student_row() == -1) {
        MessageBox("请勾选学生", "提示", MB_OK);
        return;
    }
    CString student_id;
    CString sql;
    int flag = 0;
    for (int i = 0; i < m_RSubAll.GetItemCount(); i++) {
        if (m_RSubAll.GetCheck(i)) {
            CString course_num = m_RSubAll.GetItemText(i, 1);
            
            for (int j = 0; j < m_list.GetItemCount(); j++) {
                if (m_list.GetCheck(j)) {
                    student_id = m_list.GetItemText(j, 1);
                    sql.Format("SELECT course_num FROM id_subjects WHERE course_num=%s AND student_id='%s' ", course_num, student_id);
                    if (g_dataBase.query(sql, SEL)) {
                        if (g_dataBase.num_rows > 0) continue;
                    }
                    sql.Format("INSERT INTO id_subjects VALUE(NULL,'%s',%d,0,0,0)", student_id, atoi(course_num));
                    if (g_dataBase.query(sql, ADD)) {
                        flag = 1;
                    }
                }
            }
            
        }
    }
    if (flag) {
        update_list();
        insert_per_score(m_student_num);
    }
}



void CRootDlg::OnBnClickedBtnSearchStu()
{
    CString search,temp;
    GetDlgItemText(IDC_EDIT_SEARCH_STU, temp);
    search = "SELECT * FROM student_info WHERE student_id LIKE '%#%'";
    search.Replace("#", temp);

    if (temp.IsEmpty()) {
        delete_list_all_colums();
        insert_student_header();
        update_list();
        return;
    }
    if (g_dataBase.query(search, SEL)) {
        if (g_dataBase.num_rows > 0) {
            TRACE("#list row:%d col:%d", g_dataBase.num_rows, g_dataBase.num_fields);
            delete_list_all_colums();
            insert_student_header();
            update_list_search();
        }
        else
        {
            search = "SELECT * FROM student_info WHERE username LIKE '%#%'";
            search.Replace("#", temp);
            if (g_dataBase.query(search, SEL)) {
                if (g_dataBase.num_rows > 0) {
                    delete_list_all_colums();
                    insert_student_header();
                    update_list_search();
                }
                else
                {
                    search = "SELECT u.student_id,u.username,g.score,g.my_credit \
FROM id_subjects g,student_info u \
WHERE g.student_id = u.student_id AND g.course_num = #";
                    search.Replace("#", temp);
                    g_dataBase.query(search, SEL);
                        if (g_dataBase.num_rows > 0) {
                            delete_list_all_colums();
                            insert_search_header();
                            update_list_search_class();
                        }
                        else
                        {
                            search = " SELECT u.student_id,u.username,g.score,g.my_credit \
 FROM id_subjects g, student_info u, subjects s \
 WHERE g.student_id = u.student_id AND g.course_num = s.course_num AND s.subject_name = '#'";
                            search.Replace("#", temp);
                            if (g_dataBase.query(search, SEL)) {
                                if (g_dataBase.num_rows > 0) {
                                    delete_list_all_colums();
                                    insert_search_header();
                                    update_list_search_class();
                                }
                            }
                        }
                    
                }
            }

        }
    }
}


void CRootDlg::OnMenuDel()
{
    CString sql, course;
    int flag = 0;
    for (int i = 0; i < m_ListShow.GetItemCount(); i++) {
        if (m_ListShow.GetCheck(i)) {
            course = m_ListShow.GetItemText(i, 1);
            sql.Format("DELETE FROM id_subjects WHERE student_id=%s AND course_num=%s",
                m_student_num, course);
            if (g_dataBase.query(sql, DEL)) {
                flag = 1;
            }
        }
    }
    if (flag) {
        update_list();
        insert_per_score(m_student_num);
    }
}


void CRootDlg::OnNMRClickListShow(NMHDR *pNMHDR, LRESULT *pResult)
{
    LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
    //下面的这段代码, 不单单适应于ListCtrl
    CMenu menu, *pPopup;
    menu.LoadMenu(IDR_MENU1);
    pPopup = menu.GetSubMenu(0);
    CPoint myPoint;
    ClientToScreen(&myPoint);
    GetCursorPos(&myPoint); //鼠标位置
    pPopup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, myPoint.x, myPoint.y, this);

    *pResult = 0;
}
