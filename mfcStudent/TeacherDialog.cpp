#include "stdafx.h"
#include "TeacherDialog.h"
#include"resource.h"

CTeacherDialog::CTeacherDialog()
{
}

CTeacherDialog::CTeacherDialog(UINT nIDD, CWnd * pParent)
    :CBasicDialog(nIDD,pParent)
{

}

CTeacherDialog::CTeacherDialog(UINT nIDD, CRect rect, CWnd * pParent)
    :CBasicDialog(nIDD,rect)
{
}

CTeacherDialog::CTeacherDialog(UINT nIDD, int l, int t, int r, int b, CWnd * pParent)
    :CBasicDialog(nIDD,l,t,r,b,pParent)
{
}


CTeacherDialog::~CTeacherDialog()
{
}


BOOL CTeacherDialog::OnInitDialog()
{
    CBasicDialog::OnInitDialog();

    CStringList strlist;
    strlist.AddTail("学号");
    strlist.AddTail("姓名");
    strlist.AddTail("成绩");
    strlist.AddTail("学分");
    strlist.AddTail("是否重修");

    InsertHeader(strlist, WIDTH);
    init_dialog();
    list_update();
    return TRUE;  // return TRUE unless you set the focus to a control
                  // 异常: OCX 属性页应返回 FALSE
}

void CTeacherDialog::init_dialog()
{
    vector<int> tmp = { 2,4 };
    SeteditableColums(tmp);

    CString sql;
    sql.Format("SELECT s.course_num, s.subject_name,t.account,s.credits \
        FROM subjects s,teacher t WHERE s.course_num=t.course_num AND t.account='%s'", g_teacher);
    if (g_dataBase.query(sql, SEL)) {
        CString str;
        str.Format("课程-%s(%s,%s)-学分:%s",
            g_dataBase.m_dataRow[1], g_dataBase.m_dataRow[0], g_dataBase.m_dataRow[2], g_dataBase.m_dataRow[3]);
        m_score = atof(g_dataBase.m_dataRow[3]);
        m_courseNum = atoi(g_dataBase.m_dataRow[0]);
        GetDlgItem(IDC_STATIC)->SetWindowText(str);

    }
}

void CTeacherDialog::list_update()
{
    m_list.DeleteAllItems();
    CString sql;
    sql.Format("SELECT u.student_id,u.username,tmp.score,tmp.my_credit,tmp.is_reset FROM \
student_info u,id_subjects tmp, teacher t WHERE tmp.student_id = u.student_id AND \
t.course_num = tmp.course_num AND t.account = '%s'", g_teacher);
    if (g_dataBase.query(sql, SEL)) {
        for (int i = 0; i < g_dataBase.num_rows; i++) {
            m_list.InsertItem(LVIF_TEXT | LVIF_STATE, i, "item", LVIS_SELECTED, LVIS_SELECTED, 0, 0);
            for (int j = 0; j < g_dataBase.num_fields - 1; j++) {
                m_list.SetItemText(i, j, g_dataBase.m_dataRow[j]);
            }
            m_list.SetItemText(i, 4, atoi(g_dataBase.m_dataRow[4]) ? "是" : "否");
            g_dataBase.mysql_fetch_next_row();
        }
    }
}
BEGIN_MESSAGE_MAP(CTeacherDialog, CBasicDialog)
    
    
END_MESSAGE_MAP()

void CTeacherDialog::OnEnKillfocusEditShow()
{
    CString tem;
    m_edit.GetWindowText(tem);    //得到用户输入的新的内容
    m_list.SetItemText(m_Row, m_Col, tem);   //设置编辑框的新内容
    m_edit.ShowWindow(SW_HIDE);                //应藏编辑框

    if (m_editStr == tem) return;
    CString sql,tmp,stu_id;
    stu_id = m_list.GetItemText(m_Row, 0);
    if (m_Col == 2) {
        tmp=m_list.GetItemText(m_Row, 2);
        sql.Format("UPDATE id_subjects SET score=%f WHERE student_id=%s AND course_num=%d",
            atof(tmp), stu_id, m_courseNum);
        if (g_dataBase.query(sql, ALE)) {
            sql.Format("UPDATE id_subjects SET my_credit =%f WHERE \
                student_id=%s AND course_num=%d",
                atof(tmp) >= 60 ? m_score : 0, stu_id, m_courseNum);
            if (g_dataBase.query(sql, ALE)) {
                MessageBox("修改成功", "提示", MB_OK);
                list_update();
            }
        }
    }
    else if (m_Col == 4) {
        tmp = m_list.GetItemText(m_Row, 4);
        if (tmp != "是"&& tmp != "否") {
            MessageBox("修改修改非法", "提示", MB_OK);
            return;
        }
        sql.Format("UPDATE id_subjects SET is_reset=%d WHERE student_id=%s AND course_num=%d",
            tmp=="是"?1:0, stu_id, m_courseNum);
        if (g_dataBase.query(sql, ALE)) {
            MessageBox("修改成功", "提示", MB_OK);
            list_update();
        }
    }
}


