#include "stdafx.h"
#include "DataBase.h"


CDataBase::CDataBase()
{
    num_rows = 0;
    num_fields = 0;
    
}


CDataBase::~CDataBase()
{
    mysql_close(m_mysql);
}

BOOL CDataBase::init_database()
{
    m_mysql=mysql_init(NULL);
    
    //连接数据库
    if (!mysql_real_connect(
        m_mysql,
        "127.0.0.1",
        "root",
        "qsq123",
        "student_db",
        3306,
        0,
        0))
    {
        TRACE(_T("#:数据库连接失败！"));
        const char* p = mysql_error(m_mysql);
        CString err = p;
        TRACE(_T("#query failure:%s"), err.GetBuffer());
        TRACE(_T("#errno:%d"), mysql_errno(m_mysql));
        MessageBox(nullptr, "数据库连接失败", "请检查你的数据库连接", MB_OK);
       
        return FALSE;
    }
    TRACE(_T("#:数据库连接成功！"));
    return TRUE;
}

BOOL CDataBase::query(CString sql, SQL type)
{
  
    TRACE(_T("#query sql:%s"), sql.GetBuffer());
    int res = -1;
    switch (type)
    {
    case ALE:
    case ADD:
    case DEL:
    {
        res = mysql_query(m_mysql,sql.GetBuffer());
        if (res == 0)
        {
            return TRUE;
        }
        else
        {
            const char* p = mysql_error(m_mysql);
            CString err = p;
            TRACE(_T("#query failure:%s"), err.GetBuffer());
            TRACE(_T("#errno:%d"), mysql_errno(m_mysql));
            return FALSE;
        }
    }
        break;
    case SEL:
    {
        mysql_query(m_mysql, "SET NAMES 'GB2312'");
        res = mysql_query(m_mysql, sql.GetBuffer());
        if (res == 0) {         //执行成功
            TRACE(_T("#query success!"));
            mysql_res = mysql_store_result(m_mysql);
            num_rows = mysql_num_rows(mysql_res);   //获取行
            num_fields = mysql_num_fields(mysql_res);   //获取列
            m_dataRow = mysql_fetch_row(mysql_res);
            return TRUE;
        }
        else      //执行失败
        {
            char buf[BUFSIZ] = { 0 };
            const char* p = mysql_error(m_mysql);
            CString err = p;
            TRACE(_T("#errno:%d"), mysql_errno(m_mysql));
            TRACE(_T("#errno:%s"), mysql_error(m_mysql));
            return FALSE;
        }
    }
        break;
    default:
        break;
    }
}

BOOL CDataBase::mysql_fetch_next_row()
{
    m_dataRow = mysql_fetch_row(mysql_res);
    return TRUE;
}

BOOL CDataBase::is_has_item(CString table, CString item,CString value)
{
    CString sql;
    sql.Format("SELECT %s FROM %s WHERE %s=%d", item, table, item, atoi(value));
    if (g_dataBase.query(sql, SEL)) {
        if (g_dataBase.num_rows > 0)
            return TRUE;
        else
        {
            return FALSE;
        }
    }
    return 0;
}

BOOL CDataBase::mysql_free()
{
    do
    {
        if (mysql_res != NULL)
            mysql_free_result(mysql_res);
    } while (!mysql_next_result(m_mysql));

    return TRUE;
}


