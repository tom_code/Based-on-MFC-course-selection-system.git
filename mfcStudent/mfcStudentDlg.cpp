
// mfcStudentDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "mfcStudent.h"
#include "mfcStudentDlg.h"
#include "afxdialogex.h"
#include"RootDlg.h"
#include"DataBase.h"
#include"StudentDlg.h"
#include"TeacherDialog.h"
#include"resource.h"
CDataBase g_dataBase;   //数据库全局变量
CString g_logStudent;       //登录学号
CString g_teacher;      //登录老师

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CmfcStudentDlg 对话框



CmfcStudentDlg::CmfcStudentDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_MFCSTUDENT_DIALOG, pParent)
    , m_name(_T("root"))
    , m_passwd(_T("123456"))
   
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_ICON);
}

void CmfcStudentDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialogEx::DoDataExchange(pDX);
    DDX_Text(pDX, IDC_EDIT_NAME, m_name);
    DDX_Text(pDX, IDC_EDIT_PWD, m_passwd);
    
}

BEGIN_MESSAGE_MAP(CmfcStudentDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
    ON_BN_CLICKED(IDOK, &CmfcStudentDlg::OnBnClickedOk)
    ON_BN_CLICKED(IDCANCEL, &CmfcStudentDlg::OnBnClickedCancel)
    
    ON_EN_KILLFOCUS(IDC_EDIT_NAME, &CmfcStudentDlg::OnEnKillfocusEditName)
END_MESSAGE_MAP()


// CmfcStudentDlg 消息处理程序

BOOL CmfcStudentDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();
    SetBackgroundColor(RGB(254, 212, 0));
	// 设置此对话框的图标。  当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	ShowWindow(SW_SHOW);
    int x = GetSystemMetrics(SM_CXSCREEN);
    int y = GetSystemMetrics(SM_CYSCREEN);
    CRect rect;
    this->GetWindowRect(&rect);

    //SetWindowPos(NULL, x / 2 - rect.Width() / 2, y / 2 - rect.Height() / 2, rect.Width(), rect.Height(),SWP_DRAWFRAME);
    MoveWindow(x / 2 - rect.Width() / 2, y / 2 - rect.Height() / 2, rect.Width(), rect.Height(), TRUE);
    UpdateWindow();

    ((CButton*)GetDlgItem(IDC_RADIO_USER))->SetCheck(TRUE);
    g_dataBase.init_database();

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。  对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CmfcStudentDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CmfcStudentDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CmfcStudentDlg::OnBnClickedOk()
{
    GetDlgItemText(IDC_EDIT_NAME, m_name);
    GetDlgItemText(IDC_EDIT_PWD, m_passwd);
    if (m_name.IsEmpty() || m_passwd.IsEmpty())
    {
        MessageBox(_T("请输入账号或密码"),_T("提示"));
        return;
    }
    UINT m_userChoice = GetCheckedRadioButton(IDC_RADIO_ROOT, IDC_RADIO_USER);
    switch (m_userChoice)
    {
    case IDC_RADIO_ROOT:        //管理员登录
    {
        if (m_name == "root" && m_passwd == "123456")
        {
            CRootDlg rootDlg;
            EndDialog(IDOK);
            rootDlg.DoModal();
        }
        else
        {
            MessageBox(_T("用户名或密码错误"), _T("提示"), MB_OK);
            return;
        }
    }
    break;
    case IDC_RADIO_TEA: //教师登录
    {
        TRACE("#:teacher login");
        CString sql;
        sql.Format("SELECT course_num FROM teacher WHERE account='%s'", m_name);
        if (g_dataBase.query(sql, SEL)) {
            if (m_passwd == g_dataBase.m_dataRow[0]) {
                g_teacher = m_name;
                EndDialog(IDOK);
                CTeacherDialog teacherDlg(IDD_DLG_TEACHER,0,44);
                teacherDlg.DoModal();
                TRACE("#:%d", GetLastError());
            }
            else
            {
                MessageBox(_T("用户名或密码错误"), _T("提示"), MB_OK);
                return;
            }
        }
    }
    break;
    case IDC_RADIO_USER: //学生登录
    {
        CString sql;
        sql.Format("SELECT student_id,passwd FROM student_info WHERE student_id='%s';", m_name);
        if (g_dataBase.query(sql, SEL)) {    //执行成功
            if (g_dataBase.num_rows == 0) {
                MessageBox(_T("用户不存在"), _T("提示"), MB_OK);
                return;
            }
            TRACE("#query a row:%s %s", g_dataBase.m_dataRow[0], g_dataBase.m_dataRow[1]);
            if (0 == m_name.Compare(g_dataBase.m_dataRow[0])) {//学号验证
                if (0 == m_passwd.Compare(g_dataBase.m_dataRow[1])) {//密码验证
                    g_logStudent = m_name;
                    CStudentDlg stuDlg;
                    stuDlg.DoModal();
                    EndDialog(IDOK);
                }
                else
                {
                    MessageBox(_T("密码错误"), _T("提示"), MB_OK);
                    return;
                }
            }
            else
            {
                MessageBox(_T("该学号未录入"), _T("提示"), MB_OK);
                return;
            }
        }
    }
    break;
    default:
        break;
    }
 
    CDialogEx::OnOK();
}


void CmfcStudentDlg::OnBnClickedCancel()
{
    EndDialog(IDNO);
    CDialogEx::OnCancel();
}



void CmfcStudentDlg::OnEnKillfocusEditName()
{
    if (m_userChoice == IDC_RADIO_TEA) {
       
    }
}
