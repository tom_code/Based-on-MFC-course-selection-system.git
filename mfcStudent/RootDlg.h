#pragma once
#include "afxcmn.h"
#include"AddUserDlg.h"
#include"SubjectDlg.h"

#include "afxwin.h"

// CRootDlg 对话框

class CRootDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CRootDlg)

public:
	CRootDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CRootDlg();

    void update_list();
    void update_list_search();
    void update_list_search_class();
    void update_subjects();
    void insert_header();
    void insert_student_header();
    void insert_search_header();
    void delete_list_all_colums();
    void insert_per_score(CString student_id);
    void update_sum_score(CString student_id);
    void update_reset_score(CString student_id);
    void update_all_score();
    int get_student_row();
// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DLG_ROOT };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
    CListCtrl m_list;
    virtual BOOL OnInitDialog();
    virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
    afx_msg void OnNMDblclkListUser(NMHDR *pNMHDR, LRESULT *pResult);

    afx_msg void OnBnClickedBtnAdd();
    afx_msg void OnBnClickedBtnDel();       //删除学生
    afx_msg void OnBnClickedOk();
    afx_msg LRESULT OnUpdateList(WPARAM wParam, LPARAM lParam);
private:
    CAddUserDlg* m_SubaddUer;
    CSubjectDlg* m_SubjectDlg;
    

    int m_Row;
    int m_Col;
    CString m_editStr;
    CString m_student_num;
    int  m_list_click_row;
public:
    // 所有科目
    CListCtrl m_RSubAll;
    afx_msg void OnBnClickedBtnRadd();
    afx_msg void OnBnClickedBtnRdel();
    afx_msg void OnNMDblclkRsuball(NMHDR *pNMHDR, LRESULT *pResult);
    afx_msg void OnBnClickedBtnSearch();
    CListCtrl m_ListShow;
    afx_msg void OnNMClickListUser(NMHDR *pNMHDR, LRESULT *pResult);
    afx_msg void OnNMDblclkListShow(NMHDR *pNMHDR, LRESULT *pResult);
    afx_msg void OnNMClickListShow(NMHDR *pNMHDR, LRESULT *pResult);
    CEdit m_edit;
    afx_msg void OnEnKillfocusEditShow();
    afx_msg void OnBnClickedBtnAdds();      //增加课程
    afx_msg void OnBnClickedBtnSearchStu();
    afx_msg void OnMenuDel();
    afx_msg void OnNMRClickListShow(NMHDR *pNMHDR, LRESULT *pResult);
};
