//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ 生成的包含文件。
// 供 mfcStudent.rc 使用
//
#define IDD_MFCSTUDENT_DIALOG           102
#define IDP_SOCKETS_INIT_FAILED         103
#define IDR_MAINFRAME                   128
#define IDD_DLG_ROOT                    130
#define IDD_DLG_SCORE                   132
#define IDD_DLG_USER                    134
#define IDD_DIALOG1                     136
#define IDD_DLG_COUSE                   136
#define IDD_DLG_TEACHER                 141
#define IDR_ICON                        144
#define IDR_MENU1                       145
#define IDC_EDIT_NAME                   1001
#define IDC_EDIT_PWD                    1002
#define IDC_RADIO_ROOT                  1003
#define IDC_RADIO_TEA                   1004
#define IDC_RADIO_USER                  1005
#define IDC_BTN_REG                     1006
#define IDC_CHOICE                      1006
#define IDC_LIST_USER                   1007
#define IDC_BTN_ADD                     1008
#define IDC_BTN_DEL                     1009
#define IDC_BTN_ADD2                    1009
#define IDC_BTN_RADD                    1010
#define IDC_BTN_RDEL                    1011
#define IDC_BTN_SEARCH                  1012
#define IDC_BTN_DELS                    1013
#define IDC_BTN_ADDS                    1014
#define IDC_BTN_SEARCH_STU              1015
#define IDC_EDIT_SCORE                  1016
#define IDC_EDIT_RESET                  1017
#define IDC_EDIT_YEAR                   1018
#define IDC_EDIT_ARM                    1019
#define IDC_EDIT_CODE                   1020
#define IDC_EDIT_ID                     1021
#define IDC_LIST_INFO                   1022
#define IDC_RSUBALL                     1022
#define IDC_SUB_ALL                     1023
#define IDC_SUB_HAS                     1024
#define IDC_EDIT_ORDER                  1026
#define IDC_EDIT_SEARCH                 1026
#define IDC_EDIT_TEACER                 1027
#define IDC_LIST_SHOW                   1027
#define IDC_EDIT_CSCOR                  1028
#define IDC_EDIT_SEARCH_STU             1028
#define IDC_EDIT_TIME                   1029
#define IDC_EDIT_SHOW                   1029
#define IDD_MYLISTCTRL                  1040
#define IDC_STATIC_NAME                 1040
#define IDC_EDIT_DBCLICK                1041
#define IDC_RADIO_YES                   1041
#define IDC_RADIO_NO                    1042
#define ID_MENU_32771                   32771
#define ID_MENU_DEL                     32772

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        146
#define _APS_NEXT_COMMAND_VALUE         32773
#define _APS_NEXT_CONTROL_VALUE         1042
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
